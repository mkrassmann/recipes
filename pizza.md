# Pizza for the home oven

Source: https://www.youtube.com/watch?v=G-jPoROGHGE

For me the best pizza dough recipe for around 10 pizzas. 
Hydration is around 65%.

## Start (T-21h - T-29h)

### Poolish

- 500 ml water
- 500 g flour
- 10 g fresh yeast
- 5 g honey

Mix everything together.
Keep it 1 hour at room temperature.

## 1h later (T-20h - T-28h)

After that, 16 to 24 hours into the fridge.

## 16-24h later (T-3h 30m)

### The main dough

Ingredients:
- all the poolish
- 500 ml water
- 36 g salt
- 1050 g flour

1. Put the water in a big bowl.
1. Add the poolish. Melt it in the water.
1. Add half of the flower and mix it.
1. Add the salt and mix it.
1. Add the remaining flower and mix it.
1. Put the dough out of the bowl on a big enough surface.
1. Start kneeding it by hand until all ingredients are well incorporated. 
1. Cover the dough and wait for around 15 minutes.

## 15m later (T-3h)

1. Add olive oil on your hands and start forming a fluffy big ball.
1. Put olive oil on the inside of the bowl.
1. Put the dough into the bowl and close it.
1. Leave it there for around 30-60 minutes at room temperature.

## 30-60m later (T-2h)

1. Dump the bowl upside down to the kitchen surface.
1. Put olive oil on your hands.
1. From the dough, form the pizza balls. Every ball should weight around 250 g.
1.  Put the balls into a box, close it and leave them for around 2 hours at room temperature.

## 1h later (T-1h)

1.  Pre-heat the oven with a pizza stone inside.
1.  Prepare the ingredients (tomato sauce, cheese, salami, ...)

## 1h later (T-0)

1.  After 2 hours of rest, start forming pizzas from the dough balls.
1.  Put tomato sauce on the pizza and then cook it for around 5 minutes, until the edge starts getting a nice color.
1.  Now take out the pizza from the oven, put the remaining ingredients on it and then cook it until it is ready (cheese bubbling, crust looks yummy).
1.  Enjoy :-)
